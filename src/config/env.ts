import * as dotenv from 'dotenv';
import { existsSync } from 'fs';
import { mandatories } from '.';
import logger from '../shared/utils/logger';

export function loadEnv(file = '.env') {
  if (existsSync(file)) {
    logger.debug(`[env] Loading file ${file}`);
    dotenv.config({ path: file });
  } else {
    logger.warn(
      `[env] ${file} file is missing. Did you declare the necessary variables elsewhere?`
    );
  }
  logger.debug('[env] Checking process.env for missing variables');
  for (let index = 0; index < mandatories.length; index++) {
    const element = process.env[mandatories[index]];
    if (!element) {
      logger.error(`[env] Missing variable "${mandatories[index]}"`);
      process.exit(1);
    }
    // if (element === 'sqlite') {
    //   const additionnal_element = process.env.RBH_DB_SQLITE_PATH;
    //   if (!additionnal_element) {
    //     logger.error('[env] Missing variable "RBH_DB_SQLITE_PATH"');
    //     process.exit(1);
    //   }
    // }
  }
  logger.debug(`[env] ${file} successfully loaded`);
}
