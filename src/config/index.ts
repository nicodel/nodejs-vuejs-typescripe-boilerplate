import * as dotenv from 'dotenv';
dotenv.config();
import path from 'path';

const setPublicPatch = () => {
  let publicPath = path.join(__dirname, '../public'); // 'production'
  if (process.env.NODE_ENV === 'development') {
    publicPath = path.join(__dirname, '../tmp');
  } else if (process.env.NODE_ENV === 'test') {
    publicPath = path.join(__dirname, '../public');
  }
  return publicPath;
};

const CONFIG = {
  ENV: process.env.NODE_ENV ? process.env.NODE_ENV : 'production',
  PUBLIC_PATH: setPublicPatch(),
  API_URL: '/api/v1',
  APP_SECRET: process.env.RBH_APP_SECRET as string,
  TOKEN_EXPIRY_TIME: 3600,
  IS_PRODUCTION: process.env.NODE_ENV === 'production' ? true : (false as boolean),
  REDIS_HOST: process.env.RBH_REDIS_HOST,
  REDIS_PORT: process.env.RBH_REDIS_PORT,
  REDIS_URL: `redis://${process.env.RBH_REDIS_HOST}:${process.env.RBH_REDIS_PORT}` as string,
  REDIS_TIMEOUT: 10 as number,
  DB_TYPE: process.env.RBH_DB_TYPE as string,

  // SQLite database type
  DB_SQLITE_PATH:
    process.env.RBH_DB_TYPE === 'sqlite' ? (process.env.RBH_DB_SQLITE_PATH as string) : null,

  // MariaDB or PostgreSQL database type
  DB_HOST:
    process.env.RBH_DB_TYPE === 'mariadb' || process.env.RBH_DB_TYPE === 'postgres'
      ? (process.env.RBH_DB_HOST as string)
      : null,
  DB_PORT:
    process.env.RBH_DB_TYPE === 'mariadb' || process.env.RBH_DB_TYPE === 'postgres'
      ? (parseInt(process.env.RBH_DB_PORT, 10) as number)
      : null,
  DB_DATABASE_NAME:
    process.env.RBH_DB_TYPE === 'mariadb' || process.env.RBH_DB_TYPE === 'postgres'
      ? (process.env.RBH_DB_DATABASE_NAME as string)
      : null,
  DB_USERNAME:
    process.env.RBH_DB_TYPE === 'mariadb' || process.env.RBH_DB_TYPE === 'postgres'
      ? (process.env.RBH_DB_USERNAME as string)
      : null,
  DB_PASSWORD:
    process.env.RBH_DB_TYPE === 'mariadb' || process.env.RBH_DB_TYPE === 'postgres'
      ? (process.env.RBH_DB_PASSWORD as string)
      : null,
};

let mandatories;
if (process.env.RBH_DB_TYPE === 'sqlite') {
  mandatories = [
    'RBH_APP_SECRET',
    'RBH_REDIS_HOST',
    'RBH_REDIS_PORT',
    'RBH_DB_TYPE',
    'RBH_DB_SQLITE_PATH',
  ];
  // CONFIG['DB_SQLITE_PATH'] = process.env.RBH_DB_SQLITE_PATH as string;
} else {
  mandatories = [
    'RBH_APP_SECRET',
    'RBH_REDIS_HOST',
    'RBH_REDIS_PORT',
    'RBH_DB_TYPE',
    'RBH_DB_HOST',
    'RBH_DB_PORT',
    'RBH_DB_DATABASE_NAME',
    'RBH_DB_USERNAME',
    'RBH_DB_PASSWORD',
  ];
  // CONFIG['DB_HOST'] = process.env.RBH_DB_HOST;
  // CONFIG['DB_PORT'] = process.env.RBH_DB_PORT;
  // CONFIG['DB_DATABASE_NAME'] = process.env.RBH_DB_DATABASE_NAME;
  // CONFIG['DB_USERNAME'] = process.env.RBH_DB_USERNAME;
  // CONFIG['DB_PASSWORD'] = process.env.RBH_DB_PASSWORD;
}
export { mandatories, CONFIG };
