import { CONFIG } from './config';
import { dataSource } from './shared/infra/database';
import { Users } from './shared/infra/database/models/users';
import { UsersProfiles } from './shared/infra/database/models/usersProfiles';
import { users } from './fake_data/users';
import logger from './shared/utils/logger';
import { profiles } from './fake_data/usersProfiles';

export async function startDatabase() {
  try {
    await dataSource.initialize();
    logger.info('[database] DB TypeORM initialize success');
    if (!CONFIG.IS_PRODUCTION) {
      logger.debug('[database] Loading fake data in database.');
      const usersRepo = dataSource.getRepository(Users);
      const usersProfilesRepo = dataSource.getRepository(UsersProfiles);

      await usersProfilesRepo.delete({});
      await usersRepo.delete({});

      await usersRepo.save([users.forrest_gump, users.chloe_mccardell]);
      await usersProfilesRepo.save([profiles.forrest_gump, profiles.chloe_mccardell]);

      logger.debug('[database] Fake data added to the database.');
    }
  } catch (err) {
    logger.error('[database] DB TypeORM initialize failed %s', err);
    process.exit(1);
  }
}
