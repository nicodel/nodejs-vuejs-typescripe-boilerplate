import express from 'express';
import cookieParser from 'cookie-parser';
import { CONFIG } from './config';

import indexRouter from './shared/infra/routes/index';
import { v1Router } from './shared/infra/routes/api/v1';
import usersRouter from './shared/infra/routes/users';
import logger from './shared/utils/logger';

import session from 'express-session';
import RedisStore from 'connect-redis';
import { redisClient } from './shared/infra/cache/redisClient';
import { passport } from './shared/infra/auth/passport';

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

logger.debug('[app] CONFIG.PUBLIC_PATH %s', CONFIG.PUBLIC_PATH);
app.use(express.static(CONFIG.PUBLIC_PATH));

// Initialize store.
const redisStore = new RedisStore({
  client: redisClient,
  prefix: 'rbh:',
});
// Initialize sesssion storage.
app.use(
  session({
    store: redisStore,
    resave: false, // required: force lightweight session keep alive (touch)
    saveUninitialized: false, // recommended: only save session when data exists
    secret: CONFIG.APP_SECRET,
    cookie: {
      sameSite: CONFIG.IS_PRODUCTION ? true : false,
      secure: CONFIG.IS_PRODUCTION ? true : false, // Only send cookie over HTTPS
      httpOnly: true, // Prevent client-side JavaScript from accessing the cookie
      maxAge: 1000 * 60 * 60 * 24 * 7, // Set session expiration to 1 week
    },
  })
);
app.use(passport.initialize());
app.use(passport.session());

app.use('/', indexRouter);
app.use(CONFIG.API_URL, v1Router);
app.use('/users', usersRouter);

export default app;
