/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */

import { Result } from './Result';
import { UseCaseError } from './UseCaseError';
import logger from './../utils/logger';

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace AppError {
  export class UnexpectedError extends Result<UseCaseError> {
    public constructor(err: any) {
      super(false, {
        message: 'An unexpected error occurred.',
        error: err,
      } as UseCaseError);
      logger.error('[shared/core/AppError] An unexpected error occurred: %s', err);
    }

    public static create(err: any): UnexpectedError {
      return new UnexpectedError(err);
    }
  }
}
