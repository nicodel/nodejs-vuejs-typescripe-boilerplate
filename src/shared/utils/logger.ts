/*
 * Logging String
 *   logger.info(`[httpServer in ${env}]: listening on ${bind}`);
 * Logging Object
 *   logger.debug(`Testing warn with an ${JSON.stringify({some: 'object'})}`);
 *   logger.debug('Testing warn with an %s', { another: 'object' });
 * Logging Error
 *   logger.error('Testing error with an Error', new Error('Error passed'));
 *
 * Possible icons to be used:
 *   - info:  💡️ 📢
 *   - error: 🚨️ ❗
 *   - debug: 🔧 🧰 🛠
 */

import { createLogger, format, transports } from 'winston';
import path from 'path';

const logFormat = format.printf(
  (info) => `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`
);

const logger = createLogger({
  level: process.env.NODE_ENV === 'production' ? 'info' : 'debug',
  format: format.combine(
    format.align(),
    format.splat(),
    format.simple(),
    format.label({
      label: path.basename(require.main ? require.main.filename : ''),
    }),
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' })
  ),
  exitOnError: false,
  transports: [
    // Console Logging
    new transports.Console({
      format: format.combine(format.colorize({ all: true }), logFormat),
      handleExceptions: true,
      handleRejections: true,
    }),
    // File Logging
    new transports.File({
      format: format.combine(logFormat),
      filename: process.env.NODE_ENV === 'production' ? '/var/log/rbh.log' : 'rbh.log',
      maxsize: 102400,
      maxFiles: 3,
      tailable: true,
      zippedArchive: true,
      handleExceptions: true,
      handleRejections: true,
    }),
  ],
});

logger.on('error', function (err) {
  console.error('⁉ 🚨️ Logger encountered an error: ', err);
});

export default logger;
