/* eslint-disable @typescript-eslint/no-explicit-any */
import * as r from 'redis';
import { CONFIG } from '../../../config';
import logger from '../../utils/logger';
import { RedisErrors } from './redisErrors';

const redis: typeof r = CONFIG.ENV === 'test' ? require('redis-mock') : require('redis');

const redisClient = redis.createClient({ url: CONFIG.REDIS_URL });

if (CONFIG.ENV !== 'test') {
  redisClient.connect().catch(logger.error);
}

redisClient.on('connect', () => {
  logger.info(`[redisClient] Connection status: connected to ${CONFIG.REDIS_URL}`);
});

redisClient.on('end', (msg: any) => {
  logger.warn('[redisClient] Connection status: disconnected', msg);
  new RedisErrors.DisconnectedError(msg);
});

redisClient.on('reconnecting', () => {
  logger.info('[redisClient] Connection status: reconnecting');
});

redisClient.on('error', (err: Error) => {
  logger.error('[redisClient] Connection status: error', err);
  new RedisErrors.ConnectionError(err);
});

export { redisClient };
