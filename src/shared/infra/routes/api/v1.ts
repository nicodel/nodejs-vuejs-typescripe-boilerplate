import express from 'express';

import { authRouter } from '../../../../app/infra/routes/auth';
import { usersRouter } from '../../../../app/infra/routes/users';

const v1Router = express.Router();

v1Router.use('/auth', authRouter);
v1Router.use('/users', usersRouter);

export { v1Router };
