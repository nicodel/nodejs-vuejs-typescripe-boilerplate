import express from 'express';
import path from 'path';
import logger from '../../utils/logger';
import { CONFIG } from '../../../config';

const router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
  logger.debug('[routes /] sendFile %s', path.join(CONFIG.PUBLIC_PATH, 'index.html'));
  res.sendFile(path.join(CONFIG.PUBLIC_PATH, 'index.html'));
});

export default router;
