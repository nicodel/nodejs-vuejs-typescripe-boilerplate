import { DataSource } from 'typeorm';
import { CONFIG } from './../../../config';
import logger from './../../utils/logger';
import { Users } from './models/users';
import { UsersProfiles } from './models/usersProfiles';

let dataSource: DataSource;
const entities = [Users, UsersProfiles];

switch (CONFIG.DB_TYPE) {
  case 'sqlite':
    logger.debug('[database/typeorm] Data Source for %s ', CONFIG.DB_TYPE);
    dataSource = new DataSource({
      type: CONFIG.DB_TYPE,
      database: CONFIG.DB_SQLITE_PATH,
      entities,
      synchronize: true,
      logging: !CONFIG.IS_PRODUCTION,
    });
    break;

  case 'mariadb':
    logger.debug('[database/typeorm] Data Source for %s ', CONFIG.DB_TYPE);
    dataSource = new DataSource({
      type: CONFIG.DB_TYPE,
      host: CONFIG.DB_HOST,
      port: CONFIG.DB_PORT,
      username: CONFIG.DB_USERNAME,
      password: CONFIG.DB_PASSWORD,
      database: CONFIG.DB_DATABASE_NAME,
      entities,
      synchronize: true,
      logging: !CONFIG.IS_PRODUCTION,
    });
    break;

  case 'postgres':
    logger.debug('[database/typeorm] Data Source for %s ', CONFIG.DB_TYPE);
    dataSource = new DataSource({
      type: CONFIG.DB_TYPE,
      host: CONFIG.DB_HOST,
      port: CONFIG.DB_PORT,
      username: CONFIG.DB_USERNAME,
      password: CONFIG.DB_PASSWORD,
      database: CONFIG.DB_DATABASE_NAME,
      entities,
      synchronize: true,
      logging: !CONFIG.IS_PRODUCTION,
    });
    break;

  default:
    logger.error('[database/typeorm] Unknown database type: %s', CONFIG.DB_TYPE);
    logger.error('[database/typeorm] Supported database types are: sqlite, mariadb, postgres');
    process.exit(1);
    break;
}

logger.debug('[database/typeorm] Data Source Options: %s', dataSource.options);
export { dataSource };
