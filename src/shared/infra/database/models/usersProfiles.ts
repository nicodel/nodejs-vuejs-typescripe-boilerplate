import {
  Entity,
  PrimaryColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Users } from './users';

@Entity()
export class UsersProfiles {
  @PrimaryColumn({ type: 'varchar', unique: true, nullable: false })
  id: string;

  @Column({ type: 'text', nullable: false })
  name: string;

  @Column({ type: 'text', nullable: false })
  gender: string;

  @Column({ type: 'int', nullable: false })
  birthyear: number;

  @Column({ type: 'int', nullable: false })
  height: number;

  @Column({ type: 'decimal', nullable: false })
  weight: number;

  // @Column({ type: 'text', nullable: true })
  // avatar: string;

  @Column({ type: 'text', nullable: true })
  biography: string;

  @Column({ type: 'text', nullable: true })
  location: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToOne(() => Users)
  @JoinColumn()
  user: Users;
}
