/* eslint-disable @typescript-eslint/no-explicit-any */
import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { User } from '../../../app/domains/auth/user';
import { usersRepo } from '../../../app/repositories/auth';
import logger from '../../utils/logger';

passport.use(
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
    },
    async (email: string, password: string, done) => {
      try {
        const user = await usersRepo.getUserByEmail(email);
        if (!user || !(await user.password.comparePassword(password))) {
          logger.error('[passport] LocalStrategy - Incorrect email or password: %s', email);
          return done(null, false, { message: 'Incorrect email or password' });
        }
        logger.debug(
          '[passport] LocalStrategy - Correct email & password for User: %s',
          user.props
        );
        return done(null, user);
      } catch (err) {
        logger.error('[passport] LocalStrategy - Error: %s', err);
        if (err === 'User not found.') {
          logger.error('[passport] LocalStrategy - %s', err);
          return done(null, false, { message: 'Incorrect email or password' });
        }
        return done(null, false, { message: 'Unexpected error' });
      }
    }
  )
);

passport.serializeUser((user: User, done) => {
  logger.debug('[passport] serializeUser - userId: %s', user.id.toString());
  done(null, user.id.toString());
});

passport.deserializeUser(async (id: string, done) => {
  const user = await usersRepo.getUserById(id);
  logger.debug('[passport] deserializeUser - User for id %s: %s', id, user);
  const userId = user.id.toString();
  done(null, userId);
});

export { passport };
