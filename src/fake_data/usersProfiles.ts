const profiles = {
  forrest_gump: {
    id: 'a5a7b009-c5be-41e5-ba57-8640681c0cdc',
    name: 'Forrest Gump',
    gender: 'male',
    birthyear: 1944,
    height: 183,
    weight: 80,
    location: 'Greenbow',
    biography:
      'Forrest Alexander Gump is a fictional character and the protagonist of the 1986 novel by Winston Groom, Robert Zemeckis 1994 film of the same name, and Gump and Co., the written sequel to Groom novel.',
    user: {
      id: 'a9202021-d1fe-470c-b00f-ceb85084cbea',
      email: 'forrest@gump.rbh',
      password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
    },
  },
  chloe_mccardell: {
    id: '04a724b1-e7f2-445c-ab82-d5b419bc4462',
    name: 'Chloë McCardel',
    gender: 'female',
    birthyear: 1985,
    height: 165,
    weight: 65,
    location: 'Melbourne',
    biography:
      'McCardel past swims include forty-four solo crossings of the English Channel, including eight crossings in one season and three crossings in one week, three double-crossings in 2010, 2012 and 2017 and, in 2015, the fourth person to do a non-stop triple-crossing. She also won the 28.5-mile (46-kilometer) Manhattan Island Marathon Swim in 2010. As of 2021, she holds the world record for the longest unassisted ocean swim, at 124.4 km.',
    user: {
      id: '59cb9131-cd96-4318-8899-d7f564d63f64',
      email: 'chloe@mccardell.rbh',
      password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
    },
  },
};

export { profiles };
