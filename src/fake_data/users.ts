const users = {
  forrest_gump: {
    id: 'a9202021-d1fe-470c-b00f-ceb85084cbea',
    email: 'forrest@gump.rbh',
    password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
  },
  chloe_mccardell: {
    id: '59cb9131-cd96-4318-8899-d7f564d63f64',
    email: 'chloe@mccardell.rbh',
    password: '$2a$10$fliV17vj.HgH4IJeHLCq/eX3ABhI.ypcVUbzaxoJL/fjaNd5r8Uii', // password
  },
};

export { users };
