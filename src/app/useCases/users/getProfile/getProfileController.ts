/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';
import { BaseController } from '../../../../shared/infra/BaseController';
import logger from '../../../../shared/utils/logger';
import { ProfilesDTO } from '../../../dtos/profilesDTO';
import { ProfileMap } from '../../../mappers/users/profileMap';
import { GetProfile } from './getProfile';
import { GetProfileDTO } from './getProfileDTO';

export class GetProfileController extends BaseController {
  private useCase: GetProfile;

  constructor(useCase: GetProfile) {
    super();
    this.useCase = useCase;
  }
  async executeImpl(req: Request | any, res: Response): Promise<any> {
    logger.debug('[UseCases/getProfile] Controller - req.userId: %s', req.userId);
    const dto: GetProfileDTO = {
      user_id: req.userId,
    };

    try {
      const result = await this.useCase.execute(dto);

      if (result.isLeft()) {
        const error = result.value;

        if (
          error.errorValue().message === `Could not find a user profile by user id ${dto.user_id}.`
        ) {
          return this.ok(res);
        } else {
          return this.fail(res, error.errorValue().message);
        }
      } else {
        const profile = result.value.getValue();

        return this.ok<{ profile: ProfilesDTO }>(res, {
          profile: ProfileMap.toDTO(profile),
        });
      }
    } catch (err) {
      this.fail(res, err);
    }
  }
}
