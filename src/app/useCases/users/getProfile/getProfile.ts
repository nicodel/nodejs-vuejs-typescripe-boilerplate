/* eslint-disable @typescript-eslint/no-explicit-any */
import { AppError } from '../../../../shared/core/AppError';
import { Either, left, Result, right } from '../../../../shared/core/Result';
import { UseCase } from '../../../../shared/core/UseCase';

import { Profile } from '../../../domains/users/profile';
import { IProfilesRepo } from '../../../repositories/users/IProfilesRepo';

import { GetProfileDTO } from './getProfileDTO';
import { GetProfileErrors } from './getProfileErrors';

type Response = Either<
  GetProfileErrors.ProfileNotFoundError | AppError.UnexpectedError,
  Result<Profile>
>;

export class GetProfile implements UseCase<GetProfileDTO, Promise<Response>> {
  private usersRepo: IProfilesRepo;

  constructor(usersRepo: IProfilesRepo) {
    this.usersRepo = usersRepo;
  }

  public async execute(req: GetProfileDTO): Promise<Response> {
    let profileDetails: Profile;
    const { user_id } = req;

    try {
      try {
        profileDetails = await this.usersRepo.getProfile(user_id);
      } catch (err) {
        return left(new AppError.UnexpectedError(err));
      }

      return profileDetails === null
        ? left(new GetProfileErrors.ProfileNotFoundError(user_id))
        : right(Result.ok<Profile>(profileDetails));
    } catch (err) {
      return left(new AppError.UnexpectedError(err));
    }
  }
}
