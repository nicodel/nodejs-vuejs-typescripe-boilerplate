/* eslint-disable @typescript-eslint/no-namespace */
import { Result } from '../../../../shared/core/Result';
import { UseCaseError } from '../../../../shared/core/UseCaseError';

export namespace GetProfileErrors {
  export class ProfileNotFoundError extends Result<UseCaseError> {
    constructor(id: string) {
      super(false, {
        message: `Could not find a user profile by user id ${id}.`,
      } as UseCaseError);
    }
  }
}
