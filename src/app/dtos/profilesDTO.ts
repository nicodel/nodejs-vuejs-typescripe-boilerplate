export interface ProfilesDTO {
  name: string;
  gender: string;
  birthyear: number;
  height: number;
  weight: number;
  // avatar_id: string;
  location: string;
  biography: string;
  // privacy_profile_page: string;
  // privacy_sessions: string;
  // privacy_records: string;
  // privacy_objectives: string;
  // privacy_challenges: string;
  // privacy_map_visibility: string;
}
