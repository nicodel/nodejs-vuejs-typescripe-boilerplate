// import { Photo } from '../../../domains/photo';
import { Profile } from '../../domains/users/profile';

export interface IProfilesRepo {
  getProfile(userEmail: string): Promise<Profile>;
  // createProfile(profile: Profile): Promise<void>;
  // updateProfile(profile: Profile): Promise<void>;
  // saveAvatar(avatar: Photo): Promise<void>;
}
