import { ProfilesRepo } from './implementations/profilesRepo';
// import { Images } from './../../../../shared/infra/database/typeorm/models/images';
import { UsersProfiles } from '../../../shared/infra/database/models/usersProfiles';
import { dataSource } from '../../../shared/infra/database';

const profilesRepository = dataSource.getRepository(UsersProfiles);
// const imagesRepository = dataSource.getRepository(Images);

export const profilesRepo = new ProfilesRepo(profilesRepository /* , imagesRepository */);
