/* eslint-disable @typescript-eslint/no-explicit-any */
import logger from '../../../../shared/utils/logger';
import { Profile } from '../../../domains/users/profile';
import { ProfileMap } from '../../../mappers/users/profileMap';
import { IProfilesRepo } from '../IProfilesRepo';

export class ProfilesRepo implements IProfilesRepo {
  private repo: any;

  constructor(repo: any) {
    this.repo = repo;
  }

  public async getProfile(user_id: string): Promise<Profile> {
    try {
      logger.debug('[repositories/users] getProfile - id %s', user_id);
      const profile = await this.repo.findOneBy({ user: { id: user_id } });

      logger.debug('[repositories/users] getProfile - profile %s', profile);
      if (profile === null) {
        throw new Error(`Profile not found for id: ${user_id}`);
      }
      return ProfileMap.toDomain(profile, user_id);
    } catch (err) {
      throw new Error(err.toString());
    }
  }
}
