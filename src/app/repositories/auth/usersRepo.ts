/* eslint-disable @typescript-eslint/no-explicit-any */
import { IUsersRepo } from './IUsersRepo';
import { User } from '../../domains/auth/user';
import { Email } from '../../domains/email';
import { UsersMap } from '../../mappers/auth/usersMap';
import logger from '../../../shared/utils/logger';

export class UsersRepo implements IUsersRepo {
  private repo: any;

  constructor(repo: any) {
    this.repo = repo;
  }

  async exists(userEmail: Email): Promise<boolean> {
    logger.debug('[auth/repository/users] exist userEmail %s', userEmail.value);
    const user = await this.repo.findOneBy({
      email: userEmail.value,
    });
    logger.debug('[auth/repository/users] exist user %s', user);
    return !!user === true;
  }

  async getUserByEmail(email: Email | string): Promise<User> {
    logger.debug('[modules/auth/usersRepo] getUserByEmail');
    const rawUser = await this.repo.findOneBy({
      email: email instanceof Email ? (<Email>email).value : email,
    });
    logger.debug('[modules/auth/usersRepo] getUserByEmail - rawUser %s', rawUser);

    if (rawUser === null) {
      throw new Error('User not found.');
    }

    return UsersMap.toDomain(rawUser);
  }

  async getUserById(id: string): Promise<User> {
    const rawUser = await this.repo.findOneBy({
      id: id,
    });

    if (rawUser === null) {
      throw new Error('User not found.');
    }
    return UsersMap.toDomain(rawUser);
  }

  public async save(user: User): Promise<void> {
    const rawUser = await UsersMap.toPersistence(user);
    logger.debug('[auth/repository/users] save user %s', rawUser);

    try {
      const user = await this.repo.create(rawUser);
      await this.repo.save(user);
    } catch (err) {
      throw new Error(err.toString());
    }
  }
}
