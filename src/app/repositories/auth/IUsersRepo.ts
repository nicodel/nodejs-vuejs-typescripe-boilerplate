import { User } from '../../domains/auth/user';
import { Email } from '../../domains/email';

export interface IUsersRepo {
  exists(userEmail: Email): Promise<boolean>;
  getUserById(id: string): Promise<User>;
  getUserByEmail(email: Email | string): Promise<User>;
  save(user: User): Promise<void>;
}
