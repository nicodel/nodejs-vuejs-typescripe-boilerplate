import { dataSource } from '../../../shared/infra/database';
import { Users } from '../../../shared/infra/database/models/users';
import { UsersRepo } from './usersRepo';

const usersRepository = dataSource.getRepository(Users);

export const usersRepo = new UsersRepo(usersRepository);
