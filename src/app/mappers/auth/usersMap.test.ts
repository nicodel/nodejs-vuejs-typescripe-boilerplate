import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { UniqueEntityID } from '../../../shared/domains/UniqueEntityID';
import { Email } from '../../domains/email';
import { Password } from '../../domains/auth/password';
import { User } from '../../domains/auth/user';
import { UsersMap } from './usersMap';

const domainUser = User.create(
  {
    email: Email.create({ value: 'some@email.com' }).getValue(),
    password: Password.create({ value: 'password', hashed: true }).getValue(),
  },
  new UniqueEntityID('some complex id')
).getValue();

const persistenceUser = {
  id: 'some complex id',
  email: 'some@email.com',
  password: 'password',
  /* is_email_verified: false,
  is_admin_user: false,
  is_deleted: false, */
};

describe('Modules / Users / Mappers / UserMap', () => {
  describe('toPersistence()', () => {
    it('should return an object ready to store', async () => {
      const user = await UsersMap.toPersistence(domainUser);
      user.should.eql(persistenceUser);
    });

    it('should return an object with a hashed password, if not already hashed', async () => {
      const user = await UsersMap.toPersistence(
        User.create(
          {
            email: Email.create({ value: 'some@email.com' }).getValue(),
            password: Password.create({ value: 'password' }).getValue(),
          },
          new UniqueEntityID('some complex id')
        ).getValue()
      );
      user.password.should.be.string;
    });
  });

  describe('toDomain()', () => {
    it('should return a User object', () => {
      const user = UsersMap.toDomain({
        password: 'password',
        email: 'some@email.com',
        profile: 'profile_id',
      });
      user.should.have.keys('_id', 'props', '_domainEvents');
    });
  });
});
