/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { UniqueEntityID } from '../../../shared/domains/UniqueEntityID';
import { Email } from '../../domains/email';
import logger from '../../../shared/utils/logger';

import { User } from '../../domains/auth/user';
import { Password } from '../../domains/auth/password';

interface UsersToPersistence {
  id: string;
  email: string;
  password: string;
  /* is_email_verified: boolean;
  is_admin_user: boolean;
  is_deleted: boolean; */
}

export class UsersMap extends User {
  public static async toPersistence(user: User): Promise<UsersToPersistence> {
    let password: string = null;
    if (!!user.password === true) {
      if (user.password.isAlreadyHashed()) {
        password = user.password.value;
      } else {
        password = await user.password.getHashedValue();
      }
    }

    return {
      id: user.userId.id.toString(),
      email: user.email.value,
      password: password,
      /* is_email_verified: user.isEmailVerified,
      is_admin_user: user.isAdminUser,
      is_deleted: user.isDeleted, */
    };
  }

  public static toDomain(raw: any): User {
    logger.debug('[auth/mappers] toDomain raw %s', raw);
    const passwordResult = Password.create({
      value: raw.password,
      hashed: true,
    });

    const emailResult = Email.create({ value: raw.email });

    const userResult = User.create(
      {
        password: passwordResult.getValue(),
        email: emailResult.getValue(),
      },
      new UniqueEntityID(raw.id)
    );
    logger.debug('[auth/mappers] toDomain userResult.isSuccess %s', userResult.isSuccess);
    return userResult.isSuccess ? userResult.getValue() : null;
  }
}

// "UserId":"448d2c9f-921e-4007-8088-d5b0bf4488ac","Username":"somebody","Email":"somebody@somewhere.com","Password":"SomePassword","Description":null}]
