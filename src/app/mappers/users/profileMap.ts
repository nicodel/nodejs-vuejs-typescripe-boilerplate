/* eslint-disable @typescript-eslint/no-explicit-any */
import { UniqueEntityID } from './../../../shared/domains/UniqueEntityID';

import { ShortText } from './../../domains/shortText';
import { Height } from '../../domains/users/height';
import { Weight } from './../../domains/users/weight';
import { Birthyear } from './../../domains/users/birthyear';
import { Gender } from './../../domains/users/gender';
import { Profile } from './../../domains/users/profile';
import { ProfilesDTO } from '../../dtos/profilesDTO';
import { Result } from './../../../shared/core/Result';
import { LongText } from './../../domains/longText';
import { UniqueId } from '../../domains/uniqueId';
// import { Photo } from '../../domains/photo';
// import { Privacy } from '/../../domains/users/privacy';

interface IPersistence {
  name: string;
  gender: any;
  height: number;
  weight: number;
  birthyear: number;
  // avatar: string;
  location?: string;
  biography?: string;
  // PrivacyProfilePage: any;
  // PrivacySessions: any;
  // PrivacyRecords: any;
  // PrivacyObjectives: any;
  // PrivacyChallenges: any;
  // PrivacyMapVisibility: any;
  id: string;
  user_id: string;
}

export class ProfileMap extends Profile {
  public static toPersistence(profile: Profile, profile_id: string): IPersistence {
    return {
      name: profile.name.value,
      gender: profile.gender.value,
      birthyear: profile.birthyear.value,
      height: profile.height.value,
      weight: profile.weight.value,
      // avatar: profile.avatar ? profile.avatar.value : null,
      location: profile.location ? profile.location.value : null,
      biography: profile.biography ? profile.biography.value : null,
      // PrivacyProfilePage: profile.privacy_profile_page.value,
      // PrivacySessions: profile.privacy_sessions.value,
      // PrivacyRecords: profile.privacy_records.value,
      // PrivacyObjectives: profile.privacy_objectives.value,
      // PrivacyChallenges: profile.privacy_challenges.value,
      // PrivacyMapVisibility: profile.privacy_map_visibility.value,
      id: profile_id,
      user_id: profile.user_id.id.toString(),
    };
  }

  public static toDomain(raw: IPersistence, user_id: string): Profile {
    const nameResult = ShortText.create({ value: raw.name, name: 'name' });
    const genderResult = Gender.create({ value: raw.gender });
    const birthyearResult = Birthyear.create({ value: raw.birthyear });
    const heightResult = Height.create({ value: raw.height });
    const weightResult = Weight.create({ value: raw.weight });
    // const avatarIdResult = raw.avatar ? Photo.create(raw.avatar) : null;
    const locationResult = ShortText.create({ value: raw.location, name: 'location' });
    const biographyResult = LongText.create({ value: raw.biography, name: 'biography' });
    // const privacyProfilePageResult = Privacy.create({
    //   value: raw.PrivacyProfilePage,
    //   name: 'privacy_profile_page',
    // });
    // const PrivacySessionsResult = Privacy.create({
    //   value: raw.PrivacySessions,
    //   name: 'privacy_sessions',
    // });
    // const PrivacyRecordsResult = Privacy.create({
    //   value: raw.PrivacyRecords,
    //   name: 'privacy_records',
    // });
    // const PrivacyObjectivesResult = Privacy.create({
    //   value: raw.PrivacyObjectives,
    //   name: 'privacy_objectives',
    // });
    // const PrivacyChallengesResult = Privacy.create({
    //   value: raw.PrivacyChallenges,
    //   name: 'privacy_challenges',
    // });
    // const PrivacyMapVisibilityResult = Privacy.create({
    //   value: raw.PrivacyMapVisibility,
    //   name: 'privacy_map_visibility',
    // });
    const userResult = UniqueId.create(new UniqueEntityID(user_id));

    const domainResult = Result.combine([
      nameResult,
      genderResult,
      birthyearResult,
      heightResult,
      weightResult,
      locationResult,
      biographyResult,
      // privacyProfilePageResult,
      // PrivacySessionsResult,
      // PrivacyRecordsResult,
      // PrivacyObjectivesResult,
      // PrivacyChallengesResult,
      // PrivacyMapVisibilityResult,
      userResult,
    ]);
    if (domainResult.isFailure) {
      return null;
    }

    const profileResult = Profile.create(
      {
        name: nameResult.getValue(),
        gender: genderResult.getValue(),
        birthyear: birthyearResult.getValue(),
        height: heightResult.getValue(),
        weight: weightResult.getValue(),
        // avatar: avatarIdResult ? avatarIdResult.getValue() : null,
        location: locationResult ? locationResult.getValue() : null,
        biography: biographyResult ? biographyResult.getValue() : null,
        // privacy_profile_page: privacyProfilePageResult.getValue(),
        // privacy_sessions: PrivacySessionsResult.getValue(),
        // privacy_records: PrivacyRecordsResult.getValue(),
        // privacy_objectives: PrivacyObjectivesResult.getValue(),
        // privacy_challenges: PrivacyChallengesResult.getValue(),
        // privacy_map_visibility: PrivacyMapVisibilityResult.getValue(),
        user_id: userResult.getValue(),
      },
      new UniqueEntityID(raw.id)
    );

    return profileResult.isSuccess ? profileResult.getValue() : null;
  }

  public static toDTO(profile: Profile): ProfilesDTO {
    return {
      name: profile.name.value,
      gender: profile.gender.value,
      birthyear: profile.birthyear.value,
      height: profile.height.value,
      weight: profile.weight.value,
      // avatar_id: profile.avatarId ? profile.avatarId.id.toString() : null,
      location: profile.location ? profile.location.value : null,
      biography: profile.biography ? profile.biography.value : null,
      // privacy_profile_page: profile.privacy_profile_page.value,
      // privacy_sessions: profile.privacy_sessions.value,
      // privacy_records: profile.privacy_records.value,
      // privacy_objectives: profile.privacy_objectives.value,
      // privacy_challenges: profile.privacy_challenges.value,
      // privacy_map_visibility: profile.privacy_map_visibility.value,
    };
  }
}
