import { ValueObject } from './../../shared/domains/ValueObject';
import { Result } from './../../shared/core/Result';
import { Guard } from './../../shared/core/Guard';

interface ShortTextProps {
  value: string;
  name: string;
}

export class ShortText extends ValueObject<ShortTextProps> {
  public static minLength = 2;
  public static maxLength = 20;

  get value(): string {
    return this.props.value;
  }

  private constructor(props: ShortTextProps) {
    super(props);
  }

  public static create(props: ShortTextProps): Result<ShortText> {
    const nullGuardResult = Guard.againstNullOrUndefined(props.value, props.name);

    if (!nullGuardResult.succeeded) {
      return Result.fail<ShortText>(nullGuardResult.message);
    }

    const minGuardResult = Guard.againstAtLeast(this.minLength, props.value, props.name);
    const maxGuardResult = Guard.againstAtMost(this.maxLength, props.value, props.name);

    if (!minGuardResult.succeeded) {
      return Result.fail<ShortText>(minGuardResult.message);
    }

    if (!maxGuardResult.succeeded) {
      return Result.fail<ShortText>(maxGuardResult.message);
    }

    return Result.ok<ShortText>(new ShortText(props));
  }
}
