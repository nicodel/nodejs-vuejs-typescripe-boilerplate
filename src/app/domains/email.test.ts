import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from '../../shared/core/Result';
import { Email } from './email';

let email: Email;
let emailResult: Result<Email>;

describe('Modules / App / Domain / Email', () => {
  it('should return a email', () => {
    emailResult = Email.create({ value: 'somebody@somewhere.com' });
    emailResult.isSuccess.should.be.true;
    email = emailResult.getValue();
    email.props.should.eql({ value: 'somebody@somewhere.com' });
  });
});
