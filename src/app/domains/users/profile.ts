import { Entity } from './../../../shared/domains/Entity';
import { Result } from './../../../shared/core/Result';
import { Guard } from './../../../shared/core/Guard';

import { ShortText } from './../shortText';
// import { Email } from './../email';
import { Gender } from './gender';
import { Birthyear } from './birthyear';
import { Height } from './height';
import { Weight } from './weight';
import { UniqueId } from './../uniqueId';
import { UniqueEntityID } from '../../../shared/domains/UniqueEntityID';
import { LongText } from './../longText';
// import { Privacy } from './privacy';

export interface ProfileProps {
  name: ShortText;
  // email: Email;
  gender: Gender;
  birthyear: Birthyear;
  height: Height;
  weight: Weight;
  // avatar?: ShortText;
  location?: ShortText;
  biography?: LongText;
  // privacy_profile_page: Privacy;
  // privacy_sessions: Privacy;
  // privacy_records: Privacy;
  // privacy_objectives: Privacy;
  // privacy_challenges: Privacy;
  // privacy_map_visibility: Privacy;
  user_id: UniqueId;
}

export class Profile extends Entity<ProfileProps> {
  constructor(props: ProfileProps, id?: UniqueEntityID) {
    super(props, id);
  }

  get id(): UniqueId {
    return UniqueId.create(this._id).getValue();
  }

  get name(): ShortText {
    return this.props.name;
  }

  // get email(): Email {
  //   return this.props.email;
  // }

  get gender(): Gender {
    return this.props.gender;
  }

  get birthyear(): Birthyear {
    return this.props.birthyear;
  }

  get height(): Height {
    return this.props.height;
  }

  get weight(): Weight {
    return this.props.weight;
  }

  // get avatar(): ShortText {
  //   return this.props.avatar;
  // }

  get location(): ShortText {
    return this.props.location;
  }

  get biography(): LongText {
    return this.props.biography;
  }

  // get privacy_profile_page(): Privacy {
  //   return this.props.privacy_profile_page;
  // }

  // get privacy_sessions(): Privacy {
  //   return this.props.privacy_sessions;
  // }

  // get privacy_records(): Privacy {
  //   return this.props.privacy_records;
  // }

  // get privacy_objectives(): Privacy {
  //   return this.props.privacy_objectives;
  // }

  // get privacy_challenges(): Privacy {
  //   return this.props.privacy_challenges;
  // }

  // get privacy_map_visibility(): Privacy {
  //   return this.props.privacy_map_visibility;
  // }

  get user_id(): UniqueId {
    return this.props.user_id;
  }

  public static create(props: ProfileProps, id?: UniqueEntityID): Result<Profile> {
    const nullGuard = Guard.againstNullOrUndefinedBulk([
      { argument: props.name, argumentName: 'name' },
      // { argument: props.email, argumentName: 'email' },
      { argument: props.gender, argumentName: 'gender' },
      { argument: props.birthyear, argumentName: 'birthyear' },
      { argument: props.height, argumentName: 'height' },
      { argument: props.weight, argumentName: 'weight' },
      // { argument: props.privacy_profile_page, argumentName: 'privacy_profile_page' },
      // { argument: props.privacy_sessions, argumentName: 'privacy_sessions' },
      // { argument: props.privacy_records, argumentName: 'privacy_records' },
      // { argument: props.privacy_objectives, argumentName: 'privacy_objectives' },
      // { argument: props.privacy_challenges, argumentName: 'privacy_challenges' },
      // { argument: props.privacy_map_visibility, argumentName: 'privacy_map_visibility' },
      { argument: props.user_id, argumentName: 'user_id' },
    ]);

    if (!nullGuard.succeeded) {
      return Result.fail<Profile>(nullGuard.message);
    }

    const defaultProfileProps: ProfileProps = {
      ...props,
    };

    const profile = new Profile(defaultProfileProps, id);

    return Result.ok<Profile>(profile);
  }
}
