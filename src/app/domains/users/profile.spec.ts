import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

// import { UniqueEntityID } from './../../../shared/domains/UniqueEntityID';
import { Result } from './../../../shared/core/Result';
import { ShortText } from './../shortText';
import { Height } from './height';
import { Weight } from './weight';
import { Birthyear } from './birthyear';
import { Gender } from './gender';
import { Profile, ProfileProps } from './profile';
//import { LongText } from '../longText';
// import { UniqueId } from '../uniqueId';
import { LongText } from '../longText';
import { UniqueId } from '../uniqueId';
import { UniqueEntityID } from '../../../shared/domains/UniqueEntityID';
// import { Privacy } from './privacy';
// import logger from '../../../shared/utils/logger';

let profile: Profile;
let profileResult: Result<Profile>;

const birthyear = new Date().getFullYear() - 50;

// logger.debug(
//   `ERROR ? ${JSON.stringify(Privacy.create({ value: 'friends', name: 'privacy_profile_page' }))}`
// );

const props: ProfileProps = {
  name: ShortText.create({ value: 'Some Body', name: 'name' }).getValue(),
  gender: Gender.create({ value: 'male' }).getValue(),
  birthyear: Birthyear.create({ value: birthyear }).getValue(),
  height: Height.create({ value: 175 }).getValue(),
  weight: Weight.create({ value: 75 }).getValue(),
  // avatarId: UniqueId.create(new UniqueEntityID('385ce121-6575-474a-96b5-1b826d0d6680')).getValue(),
  // avatar: ShortText.create({ value: 'some file path', name: 'avatar' }).getValue(),
  location: ShortText.create({ value: 'Some Where City', name: 'location' }).getValue(),
  biography: LongText.create({ value: 'I came from the stars...', name: 'biography' }).getValue(),
  // privacy_profile_page: Privacy.create({
  //   value: 'friends',
  //   name: 'privacy_profile_page',
  // }).getValue(),
  // privacy_sessions: Privacy.create({ value: 'you_only', name: 'privacy_sessions' }).getValue(),
  // privacy_records: Privacy.create({ value: 'everyone', name: 'privacy_records' }).getValue(),
  // privacy_objectives: Privacy.create({ value: 'friends', name: 'privacy_objectives' }).getValue(),
  // privacy_challenges: Privacy.create({ value: 'you_only', name: 'privacy_challenges' }).getValue(),
  // privacy_map_visibility: Privacy.create({
  //   value: 'everyone',
  //   name: 'privacy_map_visibilty',
  // }).getValue(),
  user_id: UniqueId.create(new UniqueEntityID('some id')).getValue(),
};

describe('Modules / App / Domain / Users / Profile', () => {
  it('should return a profile', () => {
    profileResult = Profile.create(props);
    profileResult.isSuccess.should.be.true;
    profile = profileResult.getValue();
    profile.should.exist;
  });

  it('should return name', () => {
    profile = Profile.create(props).getValue();
    const name = profile.name;
    name.value.should.equal('Some Body');
  });

  it('should return gender', () => {
    profile = Profile.create(props).getValue();
    const gender = profile.gender;
    gender.value.should.equal('male');
  });

  it('should return height', () => {
    profile = Profile.create(props).getValue();
    const height = profile.height;
    height.value.should.equal(175);
  });

  it('should return weight', () => {
    profile = Profile.create(props).getValue();
    const weight = profile.weight;
    weight.value.should.equal(75);
  });

  // it('should return avatar', () => {
  //   profile = Profile.create(props).getValue();
  //   const avatar = profile.avatar;
  //   avatar.should.eql(avatar);
  // });

  it('should return location', () => {
    profile = Profile.create(props).getValue();
    const location = profile.location;
    location.value.should.equal('Some Where City');
  });

  it('should return biography', () => {
    profile = Profile.create(props).getValue();
    const biography = profile.biography;
    biography.value.should.equal('I came from the stars...');
  });

  // it('should return privacy_profile_page', () => {
  //   profile = Profile.create(props).getValue();
  //   const result = profile.privacy_profile_page;
  //   result.value.should.be.equal('friends');
  // });

  // it('should return privacy_sessions', () => {
  //   profile = Profile.create(props).getValue();
  //   const result = profile.privacy_sessions;
  //   result.value.should.be.equal('you_only');
  // });

  // it('should return privacy_records', () => {
  //   profile = Profile.create(props).getValue();
  //   const result = profile.privacy_records;
  //   result.value.should.be.equal('everyone');
  // });

  // it('should return privacy_objectives', () => {
  //   profile = Profile.create(props).getValue();
  //   const result = profile.privacy_objectives;
  //   result.value.should.be.equal('friends');
  // });

  // it('should return privacy_challenges', () => {
  //   profile = Profile.create(props).getValue();
  //   const result = profile.privacy_challenges;
  //   result.value.should.be.equal('you_only');
  // });

  // it('should return privacy_map_visibility', () => {
  //   profile = Profile.create(props).getValue();
  //   const result = profile.privacy_map_visibility;
  //   result.value.should.be.equal('everyone');
  // });
});
