import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from './../../../shared/core/Result';
import { Birthyear } from './birthyear';

let birthyear: Birthyear;
let birthyearResult: Result<Birthyear>;

const min = new Date().getFullYear() - 100;
const max = new Date().getFullYear();

const right_birthyear = new Date().getFullYear() - 50;
const wrong_birthyear = new Date().getFullYear() - 101;

describe('Modules / App / Domain / Users / Birthyear', () => {
  it('should return a birthyear', () => {
    birthyearResult = Birthyear.create({ value: right_birthyear });
    birthyearResult.isSuccess.should.be.true;
    birthyear = birthyearResult.getValue();
    birthyear.value.should.equal(right_birthyear);
  });

  it('should fails if birthyear is not within the last 100 years', () => {
    birthyearResult = Birthyear.create({ value: wrong_birthyear });
    birthyearResult.isSuccess.should.be.false;
  });

  it('should return an error message if birthyear is not within the last 100 years', () => {
    birthyearResult = Birthyear.create({ value: wrong_birthyear });
    birthyearResult.error.should.equal(`birthyear is not within range ${min} to ${max}.`);
  });
});
