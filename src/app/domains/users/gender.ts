import { Result } from './../../../shared/core/Result';
import { ValueObject } from './../../../shared/domains/ValueObject';
import { Guard } from './../../../shared/core/Guard';
import { GenderType } from './genderType';

export interface GenderProps {
  value: GenderType;
}

export class Gender extends ValueObject<GenderProps> {
  get value(): string {
    return this.props.value;
  }

  private constructor(props: GenderProps) {
    super(props);
  }

  private static isValidGenderType(rawGender: string): boolean {
    const male: GenderType = 'male';
    const female: GenderType = 'female';
    return rawGender === male || rawGender === female;
  }

  public static create(props: GenderProps): Result<Gender> {
    const nullGuardResult = Guard.againstNullOrUndefined(props.value, 'gender');

    if (!nullGuardResult.succeeded) {
      return Result.fail<Gender>(nullGuardResult.message);
    }

    if (!this.isValidGenderType(props.value)) {
      return Result.fail<Gender>('Gender should be either "male" or "female".');
    }
    return Result.ok<Gender>(new Gender(props));
  }
}
