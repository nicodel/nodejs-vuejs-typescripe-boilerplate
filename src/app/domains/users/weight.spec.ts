import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from '../../../shared/core/Result';
import { Weight } from '../users/weight';

let weight: Weight;
let weightResult: Result<Weight>;

describe('Modules / App / Domain / Weight', () => {
  it('should return a weight', () => {
    weightResult = Weight.create({ value: 70.5 });
    weightResult.isSuccess.should.be.true;
    weight = weightResult.getValue();
    weight.props.should.eql({
      value: 70.5,
      unit: 'kg',
    });
  });
});
