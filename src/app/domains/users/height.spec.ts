import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from './../../../shared/core/Result';
import { Height } from './height';

let height: Height;
let heightResult: Result<Height>;

describe('Modules / App / Domain / Height', () => {
  it('should return a height', () => {
    heightResult = Height.create({ value: 170 });
    heightResult.isSuccess.should.be.true;
    height = heightResult.getValue();
    height.props.should.eql({
      value: 170,
      unit: 'cm',
    });
  });
});
