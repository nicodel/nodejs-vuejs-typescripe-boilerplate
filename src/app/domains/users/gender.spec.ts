import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from './../../../shared/core/Result';
import { Gender } from './gender';

let gender: Gender;
let genderResult: Result<Gender>;

describe('Modules / App / Domain / Users / Gender', () => {
  it('should return a gender', () => {
    genderResult = Gender.create({ value: 'male' });
    genderResult.isSuccess.should.be.true;
    gender = genderResult.getValue();
    gender.props.should.eql({ value: 'male' });
  });
});
