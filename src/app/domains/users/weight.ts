import { ValueObject } from '../../../shared/domains/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface WeightProps {
  value: number;
  unit?: string;
}

export class Weight extends ValueObject<WeightProps> {
  private constructor(props: WeightProps) {
    super(props);
  }

  get value(): number {
    return this.props.value;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private static isValidFloatNumber(rawWeight: any): boolean {
    return !isNaN(parseFloat(rawWeight));
  }

  public static create(props: WeightProps): Result<Weight> {
    const nullGuardResult = Guard.againstNullOrNaN(props.value, 'weight');

    if (!nullGuardResult.succeeded) {
      return Result.fail<Weight>(nullGuardResult.message);
    }

    if (!this.isValidFloatNumber(props.value)) {
      return Result.fail<Weight>('Weight should be a number.');
    }

    const weight = new Weight({
      value: props.value,
      unit: 'kg',
    });

    return Result.ok<Weight>(weight);
  }
}
