import { ValueObject } from '../../../shared/domains/ValueObject';
import { Result } from '../../../shared/core/Result';
import { Guard } from '../../../shared/core/Guard';

interface HeightProps {
  value: number;
  unit?: string;
}

export class Height extends ValueObject<HeightProps> {
  private constructor(props: HeightProps) {
    super(props);
  }

  get value(): number {
    return this.props.value;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private static isValidFloatNumber(rawHeight: any): boolean {
    return !isNaN(parseFloat(rawHeight));
  }

  public static create(props: HeightProps): Result<Height> {
    const nullGuardResult = Guard.againstNullOrNaN(props.value, 'height');

    if (!nullGuardResult.succeeded) {
      return Result.fail<Height>(nullGuardResult.message);
    }

    if (!this.isValidFloatNumber(props.value)) {
      return Result.fail<Height>('Height should be a number.');
    }

    const height = new Height({
      value: props.value,
      unit: 'cm',
    });

    return Result.ok<Height>(height);
  }
}
