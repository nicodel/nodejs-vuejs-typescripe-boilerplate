import { ValueObject } from './../../../shared/domains/ValueObject';
import { Result } from './../../../shared/core/Result';
import { Guard } from './../../../shared/core/Guard';

interface BirthyearProps {
  value: number;
}

export class Birthyear extends ValueObject<BirthyearProps> {
  public static maxLength = new Date().getFullYear();
  public static minLength = new Date().getFullYear() - 100;

  private constructor(props: BirthyearProps) {
    super(props);
  }

  get value(): number {
    return this.props.value;
  }

  public static create(props: BirthyearProps): Result<Birthyear> {
    const nullGuardResult = Guard.againstNullOrNaN(props.value, 'birthyear');

    if (!nullGuardResult.succeeded) {
      return Result.fail<Birthyear>(nullGuardResult.message);
    }

    const rangeGuardResult = Guard.inRange(
      props.value,
      this.minLength,
      this.maxLength,
      'birthyear'
    );

    if (!rangeGuardResult.succeeded) {
      return Result.fail<Birthyear>(rangeGuardResult.message);
    }

    const birthyear = new Birthyear({
      value: props.value,
    });

    return Result.ok<Birthyear>(birthyear);
  }
}
