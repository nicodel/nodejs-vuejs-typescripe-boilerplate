import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from './../../shared/core/Result';
import { ShortText } from './shortText';

let long_text: ShortText;
let shortTextResult: Result<ShortText>;

describe('Modules / App / Domain / ShortText', () => {
  it('should pass if length is between 2 and 20.', () => {
    shortTextResult = ShortText.create({ value: 'xxx', name: 'xxx' });
    shortTextResult.isSuccess.should.be.true;
  });

  it('should fail if argument is null or undefined.', () => {
    shortTextResult = ShortText.create({ value: null, name: 'xxx' });
    shortTextResult.isSuccess.should.be.false;
  });

  it('should fail if length is less than 2.', () => {
    shortTextResult = ShortText.create({ value: 'x', name: 'xxx' });
    shortTextResult.isSuccess.should.be.false;
  });

  it('should return a error message if length is less than 2.', () => {
    shortTextResult = ShortText.create({ value: 'x', name: 'xxx' });
    shortTextResult.error.should.equal('Text xxx is not at least 2 chars.');
  });

  it('should fail if length is greater than 20.', () => {
    shortTextResult = ShortText.create({
      value: Array(22).join('x'),
      name: 'xxx',
    });
    shortTextResult.isSuccess.should.be.false;
  });

  it('should return a error message if length is greater than 20.', () => {
    shortTextResult = ShortText.create({
      value: Array(22).join('x'),
      name: 'xxx',
    });
    shortTextResult.error.should.equal('Text xxx is greater than 20 chars.');
  });

  it('should return value', () => {
    shortTextResult = ShortText.create({ value: 'xxx', name: 'xxx' });
    long_text = shortTextResult.getValue();
    long_text.value.should.equal('xxx');
  });
});
