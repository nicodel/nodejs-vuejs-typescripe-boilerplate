/* eslint-disable @typescript-eslint/no-explicit-any */

import { UniqueEntityID } from './../../shared/domains/UniqueEntityID';
import { Result } from './../../shared/core/Result';
import { Entity } from './../../shared/domains/Entity';

export class UniqueId extends Entity<any> {
  get id(): UniqueEntityID {
    return this._id;
  }

  private constructor(id?: UniqueEntityID) {
    super(null, id);
  }

  public static create(id?: UniqueEntityID): Result<UniqueId> {
    return Result.ok<UniqueId>(new UniqueId(id));
  }
}
