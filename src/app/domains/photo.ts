import { Result } from '../../shared/core/Result';
import { Entity } from './../../shared/domains/Entity';
import { Guard } from '../../shared/core/Guard';
import { readFileSync, writeFileSync } from 'fs';
import { UniqueEntityID } from '../../shared/domains/UniqueEntityID';
import { UniqueId } from './uniqueId';
import logger from '../../shared/utils/logger';

export interface PhotoProps {
  filename: string;
  type: string;
  size: number;
  path: string;
  user_id: string;
}

export interface PhotoFileProps {
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  destination: string;
  filename: string;
  path: string;
  size: number;
  user_id: string;
}

export type PhotoType = 'avatar' | 'photo';

export class Photo extends Entity<PhotoProps> {
  constructor(props: PhotoProps, id?: UniqueEntityID) {
    super(props, id);
  }

  get id(): UniqueId {
    return UniqueId.create(this._id).getValue();
  }

  get filename(): string {
    return this.props.filename;
  }

  get type(): string {
    return this.props.type;
  }

  get size(): number {
    return this.props.size;
  }

  get path(): string {
    return this.props.path;
  }
  get user_id(): string {
    return this.props.user_id;
  }

  /* private static buildPhotoPath(filename: string, originalname: string): string {
    const photoPath = `photos/${filename}`;
    return photoPath;
  } */

  public static create(photoFileProps: PhotoFileProps, id?: UniqueEntityID): Result<Photo> {
    logger.debug(`Photo : ${JSON.stringify(photoFileProps)}`);
    const fieldnameResult = Guard.againstNullOrUndefined(photoFileProps.fieldname, 'fieldname');
    if (!fieldnameResult.succeeded) {
      return Result.fail<Photo>(fieldnameResult.message);
    }

    const originalnameResult = Guard.againstNullOrUndefined(
      photoFileProps.originalname,
      'originalname'
    );
    if (!originalnameResult.succeeded) {
      return Result.fail<Photo>(originalnameResult.message);
    }

    const encodingResult = Guard.againstNullOrUndefined(photoFileProps.encoding, 'encoding');
    if (!encodingResult.succeeded) {
      return Result.fail<Photo>(encodingResult.message);
    }

    const mimetypeResult = Guard.againstNullOrUndefined(photoFileProps.mimetype, 'mimetype');
    if (!mimetypeResult.succeeded) {
      return Result.fail<Photo>(mimetypeResult.message);
    }

    const destinationResult = Guard.againstNullOrUndefined(
      photoFileProps.destination,
      'destination'
    );
    if (!destinationResult.succeeded) {
      return Result.fail<Photo>(destinationResult.message);
    }

    const filenameResult = Guard.againstNullOrUndefined(photoFileProps.filename, 'filename');
    if (!filenameResult.succeeded) {
      return Result.fail<Photo>(filenameResult.message);
    }

    const pathResult = Guard.againstNullOrUndefined(photoFileProps.path, 'path');
    if (!pathResult.succeeded) {
      return Result.fail<Photo>(pathResult.message);
    }

    const sizeResult = Guard.againstNullOrUndefined(photoFileProps.size, 'size');
    if (!sizeResult.succeeded) {
      return Result.fail<Photo>(sizeResult.message);
    }

    //const photoPath = this.buildPhotoPath(photoFileProps.filename, photoFileProps.originalname);
    const photoPath = `images/${photoFileProps.filename}`;

    const data = readFileSync(photoFileProps.path);
    writeFileSync(photoPath, data, { encoding: 'base64' });

    const props: PhotoProps = {
      filename: `${photoFileProps.filename}`,
      type: photoFileProps.fieldname,
      size: photoFileProps.size,
      path: photoPath,
      user_id: photoFileProps.user_id,
    };

    return Result.ok<Photo>(new Photo(props, id));
  }
}
