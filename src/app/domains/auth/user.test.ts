import { describe } from 'mocha';
import * as chai from 'chai';
chai.should();

import { Result } from './../../../shared/core/Result';
import { User } from './user';
import { Email } from '../email';
import { Password } from './password';
import { UniqueId } from '../uniqueId';
import { UniqueEntityID } from '../../../shared/domains/UniqueEntityID';

let user: User;
let userResult: Result<User>;

const userProps = {
  email: Email.create({ value: 'some@email.com' }).getValue(),
  password: Password.create({ value: 'password' }).getValue(),
  profile: UniqueId.create(new UniqueEntityID('profile_id')).getValue(),
};

describe('Modules / Users / Domain / User', () => {
  describe('create()', () => {
    it('should return a object, with 3 keys.', () => {
      userResult = User.create(userProps);
      user = userResult.getValue();
      user.should.have.keys('_id', 'props', '_domainEvents');
    });

    // it('should return an error.', () => {
    //   userResult = User.create({
    //     email: null,
    //     password: Password.create({ value: 'password' }).getValue(),
    //     profile: UniqueId.create(new UniqueEntityID('profile_id')).getValue(),
    //   });
    //   userResult.isSuccess.should.be.false;
    // });

    it('should return a object with isDeleted to true', () => {
      userResult = User.create({
        ...userProps,
        isDeleted: true,
      });
      user = userResult.getValue();
      user.isDeleted.should.true;
    });

    // it('should return a object with isEmailVerified to true', () => {
    //   userResult = User.create({
    //     ...userProps,
    //     isEmailVerified: true,
    //   });
    //   user = userResult.getValue();
    //   user.isEmailVerified.should.true;
    // });

    //   it('should return a object with isAdminUser to true', () => {
    //     userResult = User.create({
    //       ...userProps,
    //       isAdminUser: true,
    //     });
    //     user = userResult.getValue();
    //     user.isAdminUser.should.true;
    //   });
  });

  describe('get userId()', () => {
    it('should return userId.', () => {
      userResult = User.create(userProps);
      user = userResult.getValue();
      user.userId.toString.should.be.string;
    });
  });

  describe('get email()', () => {
    it('should return email.', () => {
      userResult = User.create(userProps);
      user = userResult.getValue();
      user.email.value.should.be.equal('some@email.com');
    });
  });

  describe('get password()', () => {
    it('should return password.', () => {
      userResult = User.create(userProps);
      user = userResult.getValue();
      user.password.value.should.be.equal('password');
    });
  });

  describe('get isDeleted()', () => {
    it('should return isDeleted.', () => {
      userResult = User.create(userProps);
      user = userResult.getValue();
      user.isDeleted.should.be.false;
    });
  });

  // describe('get isEmailVerified()', () => {
  //   it('should return isEmailVerified.', () => {
  //     userResult = User.create(userProps);
  //     user = userResult.getValue();
  //     user.isEmailVerified.should.be.false;
  //   });
  // });

  // describe('get isAdminUser()', () => {
  //   it('should return isAdminUser.', () => {
  //     userResult = User.create(userProps);
  //     user = userResult.getValue();
  //     user.isAdminUser.should.be.false;
  //   });
  // });

  describe('isLoggedIn()', () => {
    it('should return false if not tokens are not set.', () => {
      userResult = User.create(userProps);
      user = userResult.getValue();
      user.isLoggedIn().should.be.false;
    });

    it('should return true if not tokens are set.', () => {
      userResult = User.create(userProps);
      user = userResult.getValue();
      user.setAccessToken('accessToken', 'refreshToken');
      user.isLoggedIn().should.be.true;
    });
  });

  describe('setAccessToken()', () => {
    it('should set accessToken', () => {
      userResult = User.create(userProps);
      user = userResult.getValue();
      user.setAccessToken('accessToken', 'refreshToken');
      user.accessToken.should.equal('accessToken');
    });

    it('should set refreshToken', () => {
      userResult = User.create(userProps);
      user = userResult.getValue();
      user.setAccessToken('accessToken', 'refreshToken');
      user.refreshToken.should.equal('refreshToken');
    });

    // it('should set lastLogin', () => {
    //   const date = new Date().getDate();
    //   userResult = User.create(userProps);
    //   user = userResult.getValue();
    //   user.setAccessToken('accessToken', 'refreshToken');
    //   user.lastLogin.getDate().should.equal(date);
    // });
  });

  describe('delete()', () => {
    it('should set isDeleted to true', () => {
      userResult = User.create(userProps);
      user = userResult.getValue();
      user.delete();
      user.isDeleted.should.be.true;
    });

    it('should not changed isDeleted if already true', () => {
      userResult = User.create({
        ...userProps,
        isDeleted: true,
      });
      user = userResult.getValue();
      user.delete();
      user.isDeleted.should.be.true;
    });
  });
});
