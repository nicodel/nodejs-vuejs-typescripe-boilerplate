import * as bcrypt from 'bcrypt-nodejs';
import { ValueObject } from '../../../shared/domains/ValueObject';
import { Guard } from '../../../shared/core/Guard';
import { Result } from '../../../shared/core/Result';
// import logger from '../../../shared/utils/logger';

export interface IPasswordProps {
  value: string;
  hashed?: boolean;
}

export class Password extends ValueObject<IPasswordProps> {
  public static minLength = 8;

  private constructor(props: IPasswordProps) {
    // logger.debug('[auth/domains/password] constructor props %s', props);
    super(props);
  }

  get value(): string {
    return this.props.value;
  }

  /**
   * @method comparePassword
   * @desc Compares as plain-text and hashed password.
   */

  public async comparePassword(plainTextPassword: string): Promise<boolean> {
    let hashed: string;

    if (this.isAlreadyHashed()) {
      hashed = this.props.value;
      return this.bcryptCompare(plainTextPassword, hashed);
    } else {
      return this.props.value === plainTextPassword;
    }
  }

  /*
   * Check if below function can be replaced by this one :
  async checkPassword(plainPassword: string): Promise<boolean> {
    return await bcrypt.compare(plainPassword, this.password);
  }
  */
  private bcryptCompare(plainText: string, hashed: string): Promise<boolean> {
    return new Promise((resolve /*, reject*/) => {
      bcrypt.compare(plainText, hashed, (err, compareResult) => {
        if (err) return resolve(false);
        return resolve(compareResult);
      });
    });
  }

  public isAlreadyHashed(): boolean {
    return this.props.hashed;
  }

  private hashPassword(password: string): Promise<string> {
    return new Promise((resolve, reject) => {
      bcrypt.hash(password, null, null, (err, hash) => {
        if (err) return reject(err);
        resolve(hash);
      });
    });
  }

  public getHashedValue(): Promise<string> {
    return new Promise((resolve) => {
      if (this.isAlreadyHashed()) {
        return resolve(this.props.value);
      } else {
        return resolve(this.hashPassword(this.props.value));
      }
    });
  }

  public static create(props: IPasswordProps): Result<Password> {
    // logger.debug('[auth/domains/password] create props %s', props);
    const propsResult = Guard.againstNullOrUndefined(props.value, 'password');
    // logger.debug('[auth/domains/password] create propsResult %s', propsResult);

    if (!propsResult.succeeded) {
      return Result.fail<Password>(propsResult.message);
    } else {
      if (!props.hashed) {
        const minLengthResult = Guard.againstAtLeast(this.minLength, props.value, 'password');
        if (!minLengthResult.succeeded) {
          return Result.fail<Password>(minLengthResult.message);
        }
      }

      return Result.ok<Password>(
        new Password({
          value: props.value,
          hashed: !!props.hashed === true,
        })
      );
    }
  }
}
