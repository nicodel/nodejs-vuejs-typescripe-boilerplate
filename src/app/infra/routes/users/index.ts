/* eslint-disable @typescript-eslint/no-explicit-any */
import { Router, Request, Response } from 'express';
import { passport } from '../../../../shared/infra/auth/passport';
// import multer from 'multer';

import { getProfileController } from './../../../useCases/users/getProfile';
// import { updateProfileController } from './../../../useCases/users/updateProfile';

const usersRouter = Router();
// const upload = multer({ dest: 'uploads/' });
usersRouter.get('/profile', passport.authenticate('session'), (req: Request, res: Response) =>
  getProfileController.execute(req, res)
);

// usersRouter.put('/profile', [Auth.authenticate(), upload.single('avatar')], (req, res) =>
//   updateProfileController.execute(req, res)
// );

export { usersRouter };
