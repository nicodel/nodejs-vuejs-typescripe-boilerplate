/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from 'express';
import express from 'express';
import logger from '../../../../shared/utils/logger';
import { passport } from '../../../../shared/infra/auth/passport';

const authRouter = express.Router();

authRouter.post('/signin', passport.authenticate('local'), (req: Request, res: Response) => {
  logger.debug('[routes/auth] POST /signin - req: %s', req);
  return res.sendStatus(200);
});

authRouter.post('/signout', (req: Request | any, res: Response, done) => {
  logger.debug('[routes/auth] POST /signout - req.session: %s', req.session);
  req.session.user = null;
  req.logout((err: Error) => {
    if (err) {
      logger.error('[routes/auth] POST /signout Error: %s', err);
      return done(err);
    }
    res.sendStatus(200);
  });
});

export { authRouter };
