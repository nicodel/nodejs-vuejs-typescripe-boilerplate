import { defineStore } from 'pinia';

interface State {
  isAuthenticated: boolean;
  error: string | null;
}

export const useAuthUserStore = defineStore('authUser', {
  state: (): State => ({
    isAuthenticated: false,
    error: null,
  }),

  getters: {
    getIsAuthenticated: (state) => state.isAuthenticated,

    getError: (state) => state.error,
  },

  actions: {},
});
