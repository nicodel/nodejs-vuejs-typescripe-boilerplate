import { defineStore } from 'pinia';

interface State {
  name: string;
  gender: string;
  birthyear: number;
  height: number;
  weight: number;
  location: string | null;
  biography: string | null;
  email: string;
}

export const useUserProfileStore = defineStore('userProfile', {
  state: (): State => {
    return {
      name: '',
      gender: '',
      birthyear: 0,
      height: 0,
      weight: 0,
      location: null,
      biography: null,
      email: '',
    };
  },

  getters: {
    isProfileStored: (state) => {
      if (
        state.name === '' ||
        state.gender === '' ||
        state.birthyear === 0 ||
        state.height === 0 ||
        state.weight === 0 ||
        state.email === ''
      ) {
        return false;
      } else {
        return true;
      }
    },
  },

  actions: {},
});

// https://pinia.vuejs.org/cookbook/migration-vuex.html

/*
      name: profile.name.value,
      gender: profile.gender.value,
      birthyear: profile.birthyear.value,
      height: profile.height.value,
      weight: profile.weight.value,
      // avatar_id: profile.avatarId ? profile.avatarId.id.toString() : null,
      location: profile.location ? profile.location.value : null,
      biography: profile.biography ? profile.biography.value : null,
*/
