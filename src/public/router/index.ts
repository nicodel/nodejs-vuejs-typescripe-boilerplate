import { createRouter, createWebHashHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import { useAuthUserStore } from '../stores/auth.user';

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/AboutView.vue'),
  },
  {
    path: '/signin',
    name: 'SignIn',
    component: () => import('../views/SignIn.vue'),
  },
  {
    path: '/signout',
    name: 'SignOut',
    component: () => import('../views/SignOut.vue'),
  },
  {
    path: '/user/profile',
    name: 'UserProfile',
    component: () => import('../views/UserProfile.vue'),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
router.beforeEach(async (to, from) => {
  const authUserStore = useAuthUserStore();
  console.debug(
    '[Router] beforeEach authUserStore.getIsAuthenticated: %s',
    authUserStore.getIsAuthenticated
  );
  if (to.name !== 'SignIn' && !authUserStore.getIsAuthenticated) {
    return { name: 'SignIn' };
  }
});

export default router;
