class ApiService {
  constructor(axiosInstance) {
    this.axiosInstance = axiosInstance;
  }

  __get({ url, params, headers }) {
    return this.axiosInstance({
      method: 'GET',
      url: `${url}`,
      params: params ? params : null,
      headers: headers ? headers : null,
    });
  }

  __post({ url, data, params, headers }) {
    return this.axiosInstance({
      method: 'POST',
      url: `${url}`,
      data: data ? data : null,
      params: params ? params : null,
      headers: headers ? headers : null,
    });
  }

  __put({ url, data, params, headers }) {
    return this.axiosInstance({
      method: 'PUT',
      url: `${url}`,
      data: data ? data : null,
      params: params ? params : null,
      headers: headers ? headers : null,
    });
  }

  async signIn({ email, password }) {
    try {
      console.debug('[ApiService] POST /auth/signin email: %s , password: %s', email, password);
      await this.__post({
        url: '/auth/signin',
        data: { email, password },
      });
    } catch (err) {
      console.error('[ApiService] POST /auth/signin Error: %s', JSON.stringify(err));
      throw new Error('Invalid username or password');
    }
  }

  async signOut() {
    try {
      await this.__post({ url: '/auth/signout' });
      return true;
    } catch (err) {
      console.error('[ApiService] signOut Error: %s', JSON.stringify(err));
      return false;
    }
  }

  async loadUserProfile() {
    try {
      const response = await this.__get({ url: '/users/profile' });
      console.debug('[ApiService] GET /users/profile response: %s', JSON.stringify(response));
      const { profile } = response.data;
      console.debug('[ApiService] GET /users/profile profile: %s', JSON.stringify(profile));
      return profile;
    } catch (err) {
      console.error('[ApiService] GET /users/profile Error: %s', JSON.stringify(err));
      throw new Error(err);
    }
  }
}

export { ApiService };
