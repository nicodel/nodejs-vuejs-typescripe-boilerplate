import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: '/api/v1',
  withCredentials: true, // Send cookies with every request
});

/* axiosInstance.interceptors.response.use(
  function (response) {
    return response;
  },
  async (error) => {
    console.debug('[base.api] getErrorResponseHandler Error: %s', error);
    if (error.response) {
      // server error 4xx, 5xx
      console.error(
        '[base.api] getErrorResponseHandler Server Error: %s',
        JSON.stringify(error.response.data)
      );
      return Promise.reject(error.response);
    } else if (error.request) {
      // no answer from server
      console.error(
        '[base.api] getErrorResponseHandler No answer from server Error: %s',
        JSON.stringify(error.request)
      );
      return Promise.reject(error.request);
    } else {
      // error while setting up the request
      console.error(
        '[base.api] getErrorResponseHandler Error while setting up the request: %s',
        JSON.stringify(error)
      );
      return Promise.reject(error);
    }
  }
); */

const onFullfilled = (response) => response;

const onRejected = async (error) => {
  console.error('[Services/Axios] onRejected Error: %s', JSON.stringify(error));
  console.error('[Services/Axios] onRejected Error.request: %s', JSON.stringify(error.request));
  if (error.response) {
    // server error 4xx, 5xx
    console.error(
      '[Services/Axios] onRejected Server Error: %s',
      JSON.stringify(error.response.data)
    );
    const errorStatus = error.response.status;
    if (errorStatus === 400) {
      console.debug('[Services/Axios] 400 Bad Request');
    } else if (errorStatus === 401) {
      console.debug('[Services/Axios] 401 Unauthorized');
    } else if (errorStatus === 404) {
      console.debug('[Services/Axios] 404 Not Found');
    }
    return Promise.reject(error.response);
  } else if (error.request) {
    // no answer from server
    console.error(
      '[Services/Axios] onRejected No answer from server Error: %s',
      JSON.stringify(error.request)
    );
    return Promise.reject(error.request);
  } else {
    // error while setting up the request
    console.error(
      '[Services/Axios] onRejected Error while setting up the request: %s',
      JSON.stringify(error)
    );
    return Promise.reject(error);
  }
};

axiosInstance.interceptors.response.use(onFullfilled, onRejected);

export { axiosInstance, onFullfilled, onRejected };
