/* eslint-disable @typescript-eslint/no-explicit-any */

import logger from './shared/utils/logger';
import app from './app';
import http from 'http';
import { CONFIG } from './config';

export async function startServer(): Promise<void> {
  logger.debug('[server] startServer…');

  const server = http.createServer(app);

  const port = normalizePort(process.env.PORT || '3000');
  app.set('port', port);

  server.listen(port);
  server.on('error', onError);
  server.on('listening', onListening);

  function onError(error: Error | any) {
    if (error.syscall !== 'listen') {
      throw error;
    }

    const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

    switch (error.code) {
      case 'EACCES':
        logger.error(`[server]: ${bind} requires elevated privileges`);
        process.exit(1);
        break;
      case 'EADDRINUSE':
        logger.error(`[server]: ${bind} is already in use`);
        process.exit(1);
        break;
      default:
        logger.error('[server]: Unknown Error %s', error);
        throw error;
    }
  }

  function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string' ? `pipe ${addr}` : `${addr ? addr.port : '???'}`;
    logger.info(`[server]: listening on tcp/${bind} for ${CONFIG.ENV}`);
    logger.info(`[server]: running at http://127.0.0.1:${bind} for ${CONFIG.ENV}`);
  }
}

const normalizePort = (val: any) => {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
};
