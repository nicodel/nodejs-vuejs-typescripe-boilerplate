# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.3.0] - unreleased

### Added
### Changed
### Deprecated
### Removed
### Fixed
### Removed

## [0.2.0] - 2023-04-13

### Added

- Publish jobs "*_prod" are only triggered when the COMMIT_TAG is a final release (as vx.x.x) (#13).
- Publish job "container_nightly" is triggered whe COMMIT_TAG do not match a final release (vx.x.x) (#13).
- NodeJs upgraded to v18 (#27).
- Badges are availabe on Gitlab project page to provide status of Pipelines, Releases and Code coverage (#14).
- MariaDB can be used as a database (#9).
- PostgreSQL can be used as a database (#10).
- Branch `develop` is deployed on [Render](https://render.com): [Vite App](https://nodejs-vuejs-typescript-boilerplate.onrender.com) (#28)
- Gitlab CI/CD triggers `verify` jobs when a new merge request is submitted. It triggers `publish` jobs when a tag is pushed, and triggers `nightly` job every day at 01h00 UTC (#22, #12).
- Creating a dedicated docker-compose file for Prod, with the application container image hosted in the Gitlab registry.

## [0.1.0] - 2023-03-27

### Added

- Unit testing and Code coverage  (#1)
- DEV mode (#2)
- Server - Logging  (#15)
- Client - Logging (#17)
- Caching (#6).
- Server - Use [TypeORM](https://typeorm.io/) as Object–relational mapping tool (#7)
- Client - Login (#17)
- Client - HTTP Error pages (#19)
- Server - Use Sqlite for database in DEV mode (#8)

[0.3.0]: https://gitlab.com/nicodel/nodejs-vuejs-typescripe-boilerplate/-/tree/develop
[0.2.0]: https://gitlab.com/nicodel/nodejs-vuejs-typescripe-boilerplate/-/releases/v0.2.0
[0.1.0]: https://gitlab.com/nicodel/nodejs-vuejs-typescripe-boilerplate/-/releases/v0.1-20230327

<!--
Release template
## ## [x.x.x] - YYYY-MM-DD

### Added
### Changed
### Deprecated
### Removed
### Fixed
### Removed
-->