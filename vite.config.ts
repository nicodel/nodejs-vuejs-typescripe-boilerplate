/// <reference types="vitest" />
import { resolve } from 'path';
import { fileURLToPath, URL } from 'node:url';
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), vueJsx()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src/public', import.meta.url)),
    },
  },
  clearScreen: false,
  root: resolve(__dirname, 'src/public'),
  server: {
    strictPort: false,
    port: 5371,
    host: '127.0.0.1',
    proxy: {
      '/api/v1/': {
        target: 'http://127.0.0.1:3000/',
      },
    },
  },
  build: {
    outDir: resolve(__dirname, 'dist/public'),
    emptyOutDir: true,
  },
  test: {
    environment: 'jsdom',
    coverage: {
      enabled: true,
      provider: 'istanbul',
    },
  },
});
