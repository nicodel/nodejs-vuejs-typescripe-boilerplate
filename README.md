# A Boilerplate for an application using NoodeJS & VueJS (and writen int Typescript)

[](https://gitlab.com/%{project_path}/badges/develop/pipeline.svg)

https://gitlab.com/%{project_path}/badges/develop/pipeline.svg


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/nicodel/nodejs-vuejs-typescripe-boilerplate.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/nicodel/nodejs-vuejs-typescripe-boilerplate/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


# A Boilerplate for an application using NoodeJS & VueJS (and written in Typescript)

A boilerplate providing a code base to start developping an application based on [ExpressJs](https://expressjs.com/) a NodeJS framework, for the server level and [VueJs](https://vuejs.org/) for the client level.

[Demo Page](https://nodejs-vuejs-typescript-boilerplate.onrender.com)

## Architecture

I chose to host both code bases (`client/` and `server/`) in the same git repository. But they could be hosted in their own git repositories and the application would have them as `git sub-modules`.

The application is splited in 2 code bases:

- `client/` will host the code of the User Interface. No much "intelligence" will be placed there, mainly displaying and capturing user requests. It is written in [Typescript](https://www.typescriptlang.org/) using [VueJs](https://vuejs.org/) framework.
- `server/` will host the code of the application itself, and connect to the database. It is written in [Typescript](https://www.typescriptlang.org/) using [ExpressJs](https://expressjs.com/) framework.

## Environnements

This boilerplate includes 3 environments to ease the development process:

### Development environment

- Based on a Debian 11 Vagrant machine, hosting a NodeJs runtime.
- All the application components are ran inside this same machine.
- Code is hosted on dedicated branch, derivated from `develop` branch.


### Qualification environment

- Based on a Debian 11 Vagrant machine, hosting a Docker runtime.
- Application components are launched in containers.
- Code is hosted on the `develop` branch.

### Production environment

- Can be deployed, or installed on any type of machine.
- Applicatin components can ba launched inside the same machine or inside containers
- Code is hosted on the `main` branch.

## Code formatting

Source code is formated by [Prettier](https://prettier.io/), and check by [ESLint](https://eslint.org/) for both `client/` and `server/`.

`client/` and `server/` shared the same configuration file `.prettierrc.json` for Prettier.

The configuration file of ESLInt ca nnot be shared as `client/` and `server/` do not used the sames environnements, respectivley `browser` and `node`.

## Unit Testing - [[#1 Unit testing and Code coverage](https://gitlab.com/nicodel/nodejs-vuejs-typescripe-boilerplate/-/issues/1)]

### Client

Unit testing for `client/` is based on [Vitest]([)](https://vitest.dev/), [Vite](https://vitejs.dev/) native unit test framework.
Code coverage is based on [Istanbul](https://istanbul.js.org/).

Test scripts have to be located in `__tests__/` directory of the tested module, and be named as `[module].spec.ts`.

Tests are launched by the command:

```bash
# nodejs-vuejs-typescripe-boilerplate/client/
$ yarn test
yarn run v1.22.19
$ vitest --run --coverage --environment jsdom --root src/

 RUN  v0.25.8 nodejs-vuejs-typescripe-boilerplate/client/src
      Coverage enabled with istanbul

 ✓ components/__tests__/HelloWorld.spec.ts (1)

 Test Files  1 passed (1)
      Tests  1 passed (1)
   Start at  21:25:11
   Duration  1.65s (transform 505ms, setup 0ms, collect 197ms, tests 17ms)

 % Coverage report from istanbul
----------------|---------|----------|---------|---------|-------------------
File            | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
----------------|---------|----------|---------|---------|-------------------
All files       |     100 |      100 |     100 |     100 |
 HelloWorld.vue |     100 |      100 |     100 |     100 |
----------------|---------|----------|---------|---------|-------------------
Done in 2.48s.
```

### Server

Unit testing for `server/` is based on [Mocha](https://mochajs.org/).
Code coverage is based on [Istanbul](https://istanbul.js.org/).

Tests scripts have to be located in `server/tests/` directory. The modules directory hierarchy has to be respected, and files names as `[module].spec.ts`.

Tests are launched using the command:

```bash
# nodejs-vuejs-typescripe-boilerplate/server/
$ yarn test
yarn run v1.22.19
$ NODE_ENV=test nyc mocha tests/**/*.spec.ts

  App
    ✔ should return "200 OK" to GET /
    ✔ should return "200 OK" to GET /users


  2 passing (22ms)

---------------------|---------|----------|---------|---------|-------------------
File                 | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
---------------------|---------|----------|---------|---------|-------------------
All files            |   95.91 |    81.57 |   85.71 |   95.83 |
 server              |   95.83 |    81.81 |     100 |   95.83 |
  app.ts             |   95.83 |    81.81 |     100 |   95.83 | 18
 server/routes       |   93.75 |    85.71 |      75 |   93.75 |
  index.ts           |    87.5 |    85.71 |      50 |    87.5 | 10
  users.ts           |     100 |    85.71 |     100 |     100 | 3
 server/shared/utils |     100 |    76.92 |     100 |     100 |
  logger.ts          |     100 |    76.92 |     100 |     100 | 3-20
---------------------|---------|----------|---------|---------|-------------------
Done in 2.19s.
```

### Application

Tests can be launched on the application level (and run both `client/` and `server/` tests) by the command:

```bash
# nodejs-vuejs-typescripe-boilerplate/
$ yarn test
yarn run v1.22.19
$ cd client && yarn test && cd ../server && yarn test
$ vitest --run --coverage --environment jsdom --root src/
...
```

## Git Hooks

Git hooks are configured on the application level, using [Husky](https://typicode.github.io/husky/#/).

`pre-commit` hook is configured to run the `yarn lint` and `yarn pretty` at the application level.

## Logging [[#4 Logging](https://gitlab.com/nicodel/nodejs-vuejs-typescripe-boilerplate/-/issues/4)]

### Server

In `server/`, logging is done by [Winston](https://github.com/winstonjs/winston).

- [Winston Transports](https://github.com/winstonjs/winston/blob/master/docs/transports.md)
- [A Complete Guide to Winston Logging in Node.js](https://betterstack.com/community/guides/logging/how-to-install-setup-and-use-winston-and-morgan-to-log-node-js-applications/#configuring-transports-in-winston)

Differentiate logging from Production, Develomment and Test.

## Caching


## Database

## Releases

### Qualification releases

Changes are made on a dedicated branch (ex: `feature/123-new-feature`). When codde is ready to be include in the `develop` branch, it is pushed onto the Gitlab repository. This `push` will trigger a pipeline that with run the `verify` jobs (`lint` and `test`) on dedicated branch.

If the pipeline is a success, a merge request to `develop` branch can be submitted.

Merge request to `develop` branch is submitted to the repository. MR should have the following:

- only 1 commit
- `CHANGELOG.md` updated with the proposed changes.

For each merge request on the `develop` branch, pipeline is triggered to run `verify` jobs: `lint` and `test`.

Nightly builds are produced from the `develop` branch by the Gitlab CI/CD pipelines. They build and publish the following:

- docker image : <application_name>:nightly


### Production releases

Changes are made on the `develop` branch. Once a release is ready to move to Production, a merge request is made to the `main` branch. For each merge request on the `main` branch, pipeline is triggered to run `verify` jobs: `lint` and `test`.

When a release is ready to be launched, a git push with tags needs to be made to the `main` branch. This will trigger pipeline jobs to build and publish the following:

- docker images : <application_name>:<tag>, <application_name>:latest
- tarball : <application_name>-<tag>.tar.gz
- release packages : releases/<tag>

To create tags on git, use the following:

```bash
git tag v0.2.1 -m 'Summarize the release.'
git push --tags

```

## Development

### Docker

Build for Qualification:

```bash
$ docker build -f Dockerfile-qual -t project .
```

Launch:

```bash
$ docker-compose up -d
$ docker-compose -f docker-compose.qual.yml up -d
```




## Roadmap


- [ ] [ExpressJs](https://expressjs.com/) written in [Typescript](https://www.typescriptlang.org/) in `server/`.
- [ ] [VueJs](https://vuejs.org/) in  Typescript in `client/`.
- [ ] [ESLint](https://eslint.org/) & [Prettier](https://prettier.io/) configured for both `client/` and `server/`, and from root directory.
- [ ] Git hooks configured with [Husky](https://typicode.github.io/husky/#/)
- [ ] Unit testing and Code coverage #1
- [ ] Running application in DEV mode #2
- [ ] Running application in QUAL mode #5
- [ ] Building a PROD tarball of application
- [ ] Install application from source
- [ ] Install application in [Docker](https://www.docker.com/community/open-source/)
- [ ] Server - Use [Redis](https://redis.io/) for cache managment #6
- [ ] Server - Use [TypeORM](https://typeorm.io/) as Object–relational mapping tool #7
- [ ] Server - Use Sqlite for database in DEV mode #8
- [ ] Server - Use [MariaDB](https://mariadb.org/) for database in QUAL and PROD mode #9
- [ ] Server - Use [PostgreSQL](https://www.postgresql.org/) for database in QUAL and PROD mode #10
- [ ] Server - Use [Winston](https://github.com/winstonjs/winston) as log managment #4
- [ ] Use [dotenv](https://github.com/motdotla/dotenv#readme) to mnage Environement Variables for both server and client. #11
- [ ] Pipeline for branch `develop`#12
- [ ] Pipeline for branch `main` #13


- [ ] Use [Badges](https://docs.gitlab.com/ee/user/project/badges.html) to provide status for [Code Coverage](https://docs.gitlab.com/ee/ci/pipelines/settings.html#test-coverage-report-badge), [Pipeline status](https://docs.gitlab.com/ee/ci/pipelines/settings.html#pipeline-status-badge) and [Latest release](https://docs.gitlab.com/ee/ci/pipelines/settings.html#latest-release-badge). #14
